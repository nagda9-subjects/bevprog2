#include <iostream>
#include "graphics.hpp"

using namespace std;
using namespace genv;

class MultipleChoice: public Widget {
    ...
}

int main() {
    std::vector<std::string> v {
        "egyik",
        "masik",
        "harmadik",
    };
    MultipleChoice choices(v);
    // choices.add_choice("negyedik");
    // choices.remove_at(0);

    // +--------+
    // |  Egyik |
    // +--------+
    // | Masik  |
    // +--------+

    std::cout << choices.current_choice() << '\n'; // pl.: "masik"
    std::cout << choices.current_index() << '\n'; // pl.: 1
}
