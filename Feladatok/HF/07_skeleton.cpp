#include <string>
#include "Widget.hpp"
#include "graphics.hpp"


// Feladat: az alabbi Button osztaly megvalositasa
//
// Egergombbal valo klikkeleskor hivodjon meg az esemenykezelo.
// Az esemenykezelo konkret viselekedeset leszarmazassal lehessen modositani.
class Button: Widget {
private:
    // A gomb szoveget irjuk/rajzoljuk ki az ablakba,
    // ertelemszeruen a gomb sajat helyere.
    std::string title;

public:
    // esemenykezelo
    virtual void on_click(event evt);
};

// 2. feladat: demoalkalmazas keszitese, amiben legalabb 2, 
// kulonbozo viselkedesu gombot letrehozol, es megmutatjatok,
// hogy kulonbozo dolgot csinalnak.
int main() {
    vector<Button *> buttons;
    buttons.push_back(new Button1(...));
    buttons.push_back(new Button2(...));

    for (int i = 0; i < buttons.size(); ++i) {
        buttons[i]->draw();
    }
}
