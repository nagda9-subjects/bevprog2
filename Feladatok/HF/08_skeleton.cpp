#include <iostream>
#include <vector>
#include <functional>

struct Button {
    // volt eredetileg:
    // void (*callback)(); // fuggvenypointer
    std::function<void()> callback;

    // volt eredetileg:
    // ugyanez a tipusu fv. pointer: void (*callback_)()
    Button(std::function<void()> callback_) : callback(callback_) {}

    void on_click() {
        callback();
    }
};

// `float`: visszateresi tipus
// `add`: fuggveny neve
// `int x`: 1. argumentum tipusa es neve
// `int y`: 2. argumentum tipusa es neve
float add(int x, int y) {
    return x + y;
}


// Altalanos forma:
// Vissz_tipus (*nev)(arg1_tipus arg1_nev, arg2_tipus arg2_nev, ...);

void print_1() {
    std::cout << "Button 1 clicked\n";
}

void print_2() {
    std::cout << "Button 2 clicked\n";
}

/*
int main() {
    float (*add_ptr)(int x, int y) = add;

    float five = add(2, 3);
    std::cout << five << '\n';

    float seven = add_ptr(3, 4);
    std::cout << seven << '\n';

    Button b1(print_1);
    Button b2(print_2);

    b1.on_click(); // print_1-et fogja meghivni
    b2.on_click(); // print_2-t fogja meghivni

    return 0;
}
*/

////// ---------------------------

// Polimorfizmus demonstralasa

struct PolimorfButton {
    std::vector<void (*)(PolimorfButton *)> vtable;
};

struct Button1: public PolimorfButton {
};

struct Button2: public PolimorfButton {
};

void Button1_on_click(PolimorfButton *this_) {
    std::cout << "Button 1 clicked; this = " << this_ << "\n";
}

void Button2_on_click(PolimorfButton *this_) {
    std::cout << "Button 2 clicked; this = " << this_ << "\n";
}

/*
int main() {
    // Button1 *instance = new Button1();
    Button1 *instance = new Button1();
    instance->vtable.push_back(Button1_on_click);

    // instance->on_click();
    instance->vtable[0](instance);

    Button2 *instance2 = new Button2();
    instance2->vtable.push_back(Button2_on_click);

    // instance2->on_click();
    instance2->vtable[0](instance2);
}
*/

struct Counter {
private:
    int number = 0;

public:
    Counter() {}

    // void (*)(Counter *)
    void step_down() {
        this->number--;
    }

    // void (*)(Counter *)
    void step_up() {
        this->number++;
    }

    int get_number() {
        return number;
    }
};

int main() {
    Counter *cnt = new Counter();

    // A callback lambda fuggveny lesz!
    // [](){ }

    /* Ez kellene:
    void anonymous_function_1() {
        cnt->btn_down();
    }
    void anonymous_function_2() {
        cnt->btn_up();
    }
    */

    // Ehelyett lambdakat hasznalhatunk
    Button *btn_down = new Button([= /* capture by-val */](){ cnt->step_down(); });
    Button *btn_up   = new Button([& /* capture by-ref */](){ cnt->step_up(); });

    std::cout << "number = " << cnt->get_number() << '\n';

    btn_down->on_click();
    std::cout << "number = " << cnt->get_number() << '\n';

    btn_up->on_click();
    std::cout << "number = " << cnt->get_number() << '\n';

    btn_up->on_click();
    std::cout << "number = " << cnt->get_number() << '\n';
}
