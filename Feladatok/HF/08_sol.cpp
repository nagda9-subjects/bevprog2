#include <string>
#include <vector>
#include <functional>
// #include "Widget.hpp"
#include "graphics.hpp"

using namespace genv;

class Widget {
protected:
    int r, g, b;
    int x, y;
    int w, h;

public:
    virtual void set_color(int r_, int g_, int b_) {
        r = r_;
        g = g_;
        b = b_;
    }

    virtual void set_pos(int x_, int y_) {
        x = x_;
        y = y_;
    }

    virtual void set_size(int w_, int h_) {
        w = w_;
        h = h_;
    }

    virtual void event_handler(event evt) {}

    virtual void draw() {
        gout << color(r, g, b)
             << move_to(x, y)
             << box(w, h);
    }
};

// Feladat: az alabbi Button osztaly megvalositasa
//
// Egergombbal valo klikkeleskor hivodjon meg az esemenykezelo.
// Az esemenykezelo konkret viselekedeset leszarmazassal lehessen modositani.
class Button: public Widget {
private:
    // A gomb szoveget irjuk/rajzoljuk ki az ablakba,
    // ertelemszeruen a gomb sajat helyere.
    std::string title;
    std::function<void(event)> on_click;

public:
    int c;

    Button(int c_, std::function<void(event)> on_click_) : c(c_), on_click(on_click_) {}

    virtual void event_handler(event evt) {
        if (evt.type == ev_mouse && evt.button != 0
            && evt.pos_x >= x && evt.pos_x <= x + w
            && evt.pos_y >= y && evt.pos_y <= y + h
           ) {
            on_click(evt);
        }
    }
};

// 2. feladat: demoalkalmazas keszitese, amiben legalabb 2,
// kulonbozo viselkedesu gombot letrehozol, es megmutatjatok,
// hogy kulonbozo dolgot csinalnak.
int main() {
    gout.open(640, 480);

    std::vector<Widget *> widgets;
    widgets.push_back(
        new Button(82, [&](event evt) {
            if (Button *button = dynamic_cast<Button *>(widgets[0])) {
                if (evt.button == btn_left) {
                    button->set_color(button->c, button->c, button->c);
                } else if (evt.button == -btn_left) {
                    button->set_color(255, 255, 255);
                }
            }
        })
    );

    widgets.push_back(new Button(164, [&](event evt) { widgets[1]->set_color(0, 255, 0); }));

    widgets[0]->set_pos(0, 0);
    widgets[0]->set_size(100, 25);
    widgets[0]->set_color(255, 255, 255);
    widgets[1]->set_pos(540, 455);
    widgets[1]->set_size(100, 25);
    widgets[1]->set_color(0, 0, 255);

    event evt;
    while (gin >> evt) {
        if (evt.type == ev_key && evt.keycode == key_escape) {
            break;
        }

        // clear screen
        gout << move_to(0, 0) << color(0, 0, 0) << box(640, 480);

        for (int i = 0; i < widgets.size(); ++i) {
            widgets[i]->event_handler(evt);
            widgets[i]->draw();
            gout << refresh;
        }
    }

    for (int i = 0; i < widgets.size(); ++i) {
        delete widgets[i];
    }

    return 0;
}
