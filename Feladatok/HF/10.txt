10. gyakorlat: Template-ek, mélyebben

Üdv mindenkinek! A mai feladat a template-ekben való további elmélyedés lenne, ezt írásos oktatóanyagok követése után néhány önálló feladattal szeretném megtámogatni.

Első lépésként kérem, olvassátok el az alábbi anyagok egyikét, lehetőleg végig. Az, hogy melyiket választjátok, rátok van bízva: olvassatok bele mindegyiknek az elejébe, és amelyik a legszimpatikusabb, azt folytassátok.

"An Idiot's Guide to C++ Templates": Kicsit terjengős, de részletes, jó magyarázat, sok példakóddal. https://www.codeproject.com/Articles/257589/An-Idiots-Guide-to-Cplusplus-Templates-Part-1
"C++ Templates": egy rövidebb, tömörebbb, de még mindig hasznos példakódokat tartalmazó, színes-szagos leírás. https://www.programiz.com/cpp-programming/templates
"C++ Tutorial: Templates": rövidebb, de még mindig sok példakóddal, egy teljes dinamikus tömb (az std::vector-hoz hasonló) megvalósításával: https://www.bogotobogo.com/cplusplus/templates.php
"C++ Templates Tutorial": a Floridai Egyetem által készített leírás, amely az alapoktól a haladóbb template-használatig sokmindent bemutat. https://users.cis.fiu.edu/~weiss/Deltoid/vcstl/templates
A következőkben ismerkedjetek meg a C++ Standard Library két alapvető fontosságú "smart pointer" osztályával: a unique_ptr és a shared_ptr típusokkal. Ehhez a következő anyagokat javaslom:

A kanonikus Stack Overflow válasz a témában (érdemes a kérdés alatti kommentek linkjeit is megnézegetni): https://stackoverflow.com/questions/106508/what-is-a-smart-pointer-and-when-should-i-use-one
A Microsoft dokumentációja a C++ smart pointerekkel kapcsolatban: https://docs.microsoft.com/en-us/cpp/cpp/smart-pointers-modern-cpp?view=msvc-160
A unique_ptr céljáról és használatáról: https://docs.microsoft.com/en-us/cpp/cpp/how-to-create-and-use-unique-ptr-instances?view=msvc-160
Stack Overflow kérdés és válasz a referenciaszámlálással kapcsolatban: https://stackoverflow.com/questions/45080117/what-is-reference-counter-and-how-does-it-work
Wikipédia szócikk az RAII idiómáról: https://en.wikipedia.org/wiki/Resource_acquisition_is_initialization
A cppreference oldal cikke az RAII-ről: https://en.cppreference.com/w/cpp/language/raii
Ha sikerült a két standard smart pointer típus működését és használatát megérteni (javaslom egyszerű példakódok írását saját magatoknak!), akkor a…

Házi feladat: saját, RAII-alapú smart pointernek (tehát kb. a unique_ptr megfelelőjének) az írása, template-ek használatával. Csak annyi megkötés van, hogy a smart pointernek template-nek kell lennie, kell tudni nyers pointerrel vagy értékkel inicializálni (tehát legyen UniqueSmartPtr(T value) vagy UniqueSmartPtr(T *pointer) szignatúrájú konstruktora), és a destruktorban szabadítsa fel a memóriát (delete-tel). A másoló konstruktort értelemszerűen delete-elni kell (ennek elmaradása double free hibákhoz vezethet, amint azt a Microsoft fenti leírása is tárgyalja). A feladatként elkészített UniqueSmartPtr osztálynak nem kell tudnia öröklődést vagy egyedi allokátorokat kezelni, semmi ilyen sallanggal nem kell foglalkozni. A lényeg, hogy egy helyesen működő, egyszerű smart pointert készítsetek.

Azok kedvéért, akiknek javítási lehetőségre van szüksége, kiírásra kerülnek…

Szorgalmi házi feladatok, extra pontokért:

A unique_ptr mintájára referenciaszámlált smart pointer (SharedSmartPtr) megvalósítása. Ez a UniqueSmartPtr átalakításával könnyen megtehető: a másoló konstruktorban a referenciaszámláló növelését kell hozzáadni, a destruktorban pedig a csökkentését, és 0 referenciaszámláló esetén a delete-elést. Ugyanúgy nem kell öröklődéssel vagy allokátorokkal foglalkozni.
Generikus (template-es) bináris keresés írása. Nézzetek utána a bináris keresés nevű alapvető algoritmusnak, aminek célja, hogy rendezett tömbben gyorsan megtalálja egy elem helyét, indexét! Írjatok egy binary_search template függvényt, amely bármilyen elemtípusú std::vector-ban lévő elem indexét visszaadja, amennyiben a típus támogatja a <, >, == operátorokkal való összehasonlítást. A függvény szignatúrája legegyszerűbb esetben például az alábbi lehet:
int binary_search(std::vector<T> array, T elem)

Jó munkát kívánok!
