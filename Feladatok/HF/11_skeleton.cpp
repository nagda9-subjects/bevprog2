#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <iostream>
#include <algorithm>
#include <chrono>


using namespace std;

int index_of_baseline(const vector<string> &v, const string &s) {
    for (size_t i = 0; i < v.size(); ++i) {
        if (v[i] == s) {
            return i;
        }
    }

    return -1;
}

int index_of_iterator(const vector<string> &v, const string &s) {
    for (vector<string>::const_iterator it = v.begin();
         it != v.end();
         ++it)
    {
        if (*it == s) {
            // Tfh. string merete 24
            // +-----+-----+-----+
            // | foo | bar | baz |
            // +-----+-----+-----+
            // ^     ^     ^     ^
            // +-10  +-34  +-58  +-82
            //             ^
            // v.begin()   |     v.end()
            //             ^
            //             +- it
            //
            // it - v.begin() == (58 - 10) / 24 == 2
            return it - v.begin();
        }
    }
    return -1;
}

int index_of_map(const map<string, int> &m, const string &s) {
    // map<Key, Value>::find():
    // const_iterator find(const Key& key) const
    //
    // template<typename T, typename U>
    // struct pair {
    //     T first;
    //     U second;
    // };
    //
    // *m.find(s): const std::pair<string, int> &
    map<string, int>::const_iterator it = m.find(s);
    if (it == m.end()) {
        // nincs meg ez a kulcs a map-ben
        return -1;
    }

    // (*pointer).field == pointer->field
    // (*iterator).field == iterator->field
    // return (*it).second;
    return it->second;
}

int index_of_bsearch(const vector<string> &v, const string &s) {
    vector<string>::const_iterator it = lower_bound(v.begin(), v.end(), s);

    if (it == v.end()) {
        return -1;
    }

    if (*it == s) {
        return it - v.begin();
    }

    return -1;
}

int main() {
    vector<string> v {
        "foo", "bar", "baz", "alma"
    };

    cout << index_of_baseline(v, "baz") << '\n';
    cout << index_of_baseline(v, "xyz") << '\n';

    cout << index_of_iterator(v, "baz") << '\n';
    cout << index_of_iterator(v, "xyz") << '\n';

    sort(v.begin(), v.end());

    cout << index_of_bsearch(v, "baz") << '\n';
    cout << index_of_bsearch(v, "xyz") << '\n';

    map<string, int> m {
        { "foo",  0 },
        { "bar",  1 },
        { "baz",  2 },
        { "alma", 3 },
    };

    map<string, int> m2;
    m2["foo"]  = 0;
    m2["bar"]  = 1;
    m2["baz"]  = 2;
    m2["alma"] = 3;

    cout << index_of_map(m, "baz") << "\n";
    cout << index_of_map(m, "xyz") << "\n";

    // https://en.cppreference.com/w/cpp/chrono/steady_clock
    auto start_time = chrono::steady_clock::now();
    for (int i = 0; i < 10000; ++i) {
        // A merendo utasitas kerul ide
        cout << 0;
    }
    auto end_time = chrono::steady_clock::now();
    int us = chrono::duration_cast<chrono::microseconds>(end_time - start_time).count();
    cout << "took " << us << " us\n";

    // Megmerendo (10000 - 1000000 iteracioval):
    // * index_of_baseline
    // * index_of_iterator
    //
    // * index_of_map
    // * index_of_bsearch
    //
    // Ehhez termeszetesen nagymeretu tombokre van szukseg.
    // Hozzatok letre valahogyan egy legalabb par 1000 elemu tombot!
    //
    // Szorgalmi: kapcsoljatok ki es be a compiler optimalizaciokat
    // (Debug vs. Release mod) -- Ismeteljetek igy is meg a mereseket!
    // Tapasztaltok kulonbseget? Szamitanak-e az optimalizaciok?
    return 0;
}
