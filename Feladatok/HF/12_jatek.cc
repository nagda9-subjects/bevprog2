
class Widget {
    vector<Widget *> children;
    std::function<void()> callback;

    virtual bool handle_event(event evt) {
        for (auto it = children.begin(); it != children.end(); ++it) {
            if ((*it)->handle_event(evt)) {
                return true;
            }
        }
        return false;
    }

    virtual void draw() {
        for (auto it = children.begin(); it != children.end(); ++it) {
            (*it)->draw();
        }
    }
};

class FireButton: Widget {
    bool handle_event(event evt) {
        if (evt.type == ev_mouse) {
            /// lekezelem
            // this->callback();
            messenger.publish(EVENT_FIRE);

            return true;
        }
        return false;
    }
};

class TextLabel: Widget {
    TextLabel() {
        messenger.subscribe([](){ this->text = "Firing!"; }, EVENT_FIRE);
    }
    bool handle_event(event evt) {
        if (evt.type == ev_key) {
            /// lekezelem
            this->callback();
            return true;
        }
        return false;
    }
};


const int EVENT_FIRE = 1;
const int EVENT_JUMP = 2;
const int EVENT_GROW = 3;

class Messenger {
    map<int, vec<function<void()>>> handlers;

    void subscribe(std::function<void()> handler, int event_code) {
        handlers[event_code].push_back(handler);
    }

    // publisher-subscriber pattern
    void publish(int event_code) {
        for (auto it = handlers[event_code].begin();
             it != handlers[event_code].end();
             ++it
        ) {
            (*it)();
        }
    }
};

static Messenger messenger;

class GameState {
    Window *window; // a teljes kepernyore/ablakra

    // ctor
    GameState() {
        // letrehozom a widgetek hierarchiajat
        window = new window(...);
        window.children.push_back(new TextEditor());
        window.children.push_back(new Button([](){
            window.children.push_back(new TextEditor());
        }));

        messenger.subscribe([](){
            enemy.points -= 10;
            window->redraw();
        }, EVENT_FIRE);
    }

    void run() {
        event evt;
        while (gin >> evt) {
            window->handle_event(evt);
            window->draw();
        }
    }
};


int main() {
    GameState state;

    state.run();
}
