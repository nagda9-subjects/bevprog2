#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include "graphics.hpp"

using namespace std;
using namespace genv;

int SCREEN_INTRO = 0;
int SCREEN_GAME = 1;
int SCREEN_GAME_OVER = 2;
int SCREEN_LEVEL_2 = 3;


struct Point {
    int x;
    int y;
};

struct Size {
    int w;
    int h;
};

struct Rect {
    Point top_left;
    Point bottom_right;

    bool contains(Point p) {
        return top_left.x <= p.x && p.x <= bottom_right.x
            && top_left.y <= p.y && p.y <= bottom_right.y;
    }

    bool overlaps(Rect r) {
        //
        //
        //
        //        |           |
        //   1.   |    2.     |    3.
        // -------+-----------+---------
        //        |    5.     |
        //   4.   | teglalap  |    6.
        // -------+-----------+---------
        //        |           |
        //   7.   |    8.     |    9.
        //
        // (a) 1., 4., 7.: r.bottom_right.x < top_left.x
        // (b) 3., 6., 9.: r.top_left.x > bottom_right.x
        // (c) 1., 2., 3.: r.bottom_right.y < top_left.y
        // (d) 7., 8., 9.: r.top_right.y > bottom_left.y
        //
        // if (a || b || c || d) {
        //     // nincs atfedes
        // } else {
        //     // van atfedes
        // }
        //
        //                  3.
        //             +---------+
        //             |    r    |
        //        +----+------+  |
        //        |    |      |  |
        //        |    +------+--+
        //        | teglalap  |    6.
        //        +-----------+
        //
        // if (a) {
        //    // r balra van a teglalapomtol
        //    if (c) {
        //        // r feljebb van
        //    } else {
        //        // r lejjebb van
        //    }
        // } else {
        //    // egyebkent jobbra -- pl. a fenti esetben
        //    if (c) {
        //        // r feljebb van
        //        overlap_min_x = r.top_left.x
        //        overlap_max_x = bottom_right.x
        //        overlap_min_y = top_left.y
        //        overlap_max_y = r.bottom_right.y
        //        return Rect(Point(overlap_min_x, overlap_min_y),
        //                    Point(overlap_max_x, overlap_max_y));
        //    } else {
        //        // r lejjebb van
        //    }
        // }
    }
};

void intro(event evt, int &current_screen) {
    // ide jon a fokepernyo logikaja
    // 1. kirajzolom a fomenut
    gout << move_to(320, 240)
         << color(255, 255, 255)
         << text("fomenu");

    // 2. utana beallitom, hogy a kovetkezo allapot a jatek maga
    if (evt.type == ev_mouse && evt.button > 0) {
        current_screen = SCREEN_GAME;
    }
}

void main_game_loop(event evt, int &current_screen, vector<Square> &squares) {
    // ...
    // ide jon a jatek logikaja
    gout << move_to(0, 0)
         << color(255, 0, 0)
         << box(640, 480);

    if (evt.type == ev_mouse && evt.button > 0 && evt.pos_y > 320) {
        current_screen = SCREEN_GAME_OVER;
    }
}

int main() {
    // 0: intro/fomenu
    // 1: tenyleges jatek
    // 2: game over
    int current_screen = SCREEN_INTRO;
    vector<Square> squares;

    gout.open(640, 480);

    // Programtervezesi minta neve: ALLAPOTGEP, "state machine"

    event evt;
    while (gin >> evt) {
        if (current_screen == SCREEN_INTRO) {
            intro(evt, current_screen);
        } else if (current_screen == SCREEN_GAME) {
            main_game_loop(evt, current_screen, squares);
        } else if (current_screen == SCREEN_LEVEL_2) {
            // ...
        } else if (current_screen == SCREEN_GAME_OVER) {
        }

        gout << refresh;
    }
}
